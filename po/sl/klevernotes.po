# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the klevernotes package.
#
# Matjaž Jeran <matjaz.jeran@amis.net>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: klevernotes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-03 01:44+0000\n"
"PO-Revision-Date: 2024-03-01 07:23+0100\n"
"Last-Translator: Matjaž Jeran <matjaz.jeran@amis.net>\n"
"Language-Team: Slovenian <lugos-slo@lugos.si>\n"
"Language: sl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 1 : n%100==2 ? 2 : n%100==3 || n"
"%100==4 ? 3 : 0);\n"
"X-Generator: Poedit 3.4.2\n"

#: contents/ui/dialogs/colorDialog/ColorDialog.qml:11
#, kde-format
msgctxt "@title:dialog"
msgid "Color Picker"
msgstr "Kapalka"

#: contents/ui/dialogs/DeleteConfirmationDialog.qml:10
#: contents/ui/dialogs/NamingErrorDialog.qml:11
#, kde-format
msgctxt "Name, as in 'A note category'"
msgid "category"
msgstr "kategorija"

#: contents/ui/dialogs/DeleteConfirmationDialog.qml:11
#: contents/ui/dialogs/NamingErrorDialog.qml:12
#, kde-format
msgctxt "Name, as in 'A note group'"
msgid "group"
msgstr "skupina"

#: contents/ui/dialogs/DeleteConfirmationDialog.qml:12
#: contents/ui/dialogs/NamingErrorDialog.qml:13
#, kde-format
msgctxt "Name, as in 'A note'"
msgid "note"
msgstr "zabeležka"

#: contents/ui/dialogs/DeleteConfirmationDialog.qml:17
#, kde-format
msgctxt ""
"@subtitle:dialog, %1 can be 'category' (a note category), 'group' (a note "
"group) or 'note' (a note)"
msgid "Are you sure you want to delete this %1 ?"
msgstr "Ali ste prepričani, da želite izbrisati %1?"

#: contents/ui/dialogs/emojiDialog/EmojiGrid.qml:80
#, kde-format
msgid "No stickers"
msgstr "Brez nalepk"

#: contents/ui/dialogs/emojiDialog/EmojiGrid.qml:80
#, kde-format
msgid "No emojis"
msgstr "Brez čustvenčkov"

#: contents/ui/dialogs/FilePickerDialog.qml:14
#, kde-format
msgctxt "@title:dialog"
msgid "Image picker"
msgstr "Izbirnik slik"

#: contents/ui/dialogs/FileSaverDialog.qml:15
#, kde-format
msgctxt "@title:dialog, choose the location of where the file will be saved"
msgid "Save note"
msgstr "Shrani zabeležko"

#: contents/ui/dialogs/FontPickerDialog.qml:17
#, kde-format
msgctxt "@title:dialog"
msgid "Font selector"
msgstr "Izbirnik pisave"

#: contents/ui/dialogs/imagePickerDialog/ImagePickerDialog.qml:33
#, kde-format
msgctxt "@title:dialog"
msgid "Image selector"
msgstr "Izbirnik slik"

#: contents/ui/dialogs/imagePickerDialog/ImagePickerDialog.qml:81
#, kde-format
msgctxt "@label:button, As in 'image from the internet'"
msgid "Web image"
msgstr "Spletna slika"

#: contents/ui/dialogs/imagePickerDialog/ImagePickerDialog.qml:110
#, kde-format
msgctxt "label:button, Image from the local file system"
msgid "Local image"
msgstr "Krajevna slika"

#: contents/ui/dialogs/imagePickerDialog/ImagePickerDialog.qml:141
#, kde-format
msgctxt "@label:button"
msgid "Paint an image"
msgstr "Naslikajte sliko"

#: contents/ui/dialogs/imagePickerDialog/ImagePickerDialog.qml:164
#, kde-format
msgctxt "@label:button, Collection of image stored inside the note folder"
msgid "Image collection"
msgstr "Zbirka slik"

#: contents/ui/dialogs/imagePickerDialog/ImagePickerDialog.qml:236
#, kde-format
msgid ""
"It appears that the image you selected does not exist or is not supported."
msgstr "Kaže, da slika, ki ste jo izbrali, ne obstaja ali ni podprta."

#: contents/ui/dialogs/imagePickerDialog/ImagePickerDialog.qml:255
#, kde-format
msgctxt "@label:textbox, text associated to the selected image"
msgid "Image text: "
msgstr "Besedilo slike: "

#: contents/ui/dialogs/imagePickerDialog/ImagePickerDialog.qml:264
#, kde-format
msgctxt "@label:checkbox"
msgid "Place this image inside the note folder"
msgstr "Postavite to sliko znotraj mape z zabeležkami"

#: contents/ui/dialogs/LeavePaintingDialog.qml:10
#, kde-format
msgctxt "@title:dialog"
msgid "KleverNotes painting"
msgstr "Slikanje KleverNotes"

#: contents/ui/dialogs/LeavePaintingDialog.qml:11
#, kde-format
msgid ""
"You're about to leave without saving your drawing!\n"
"Do you want to save it before leaving ?\n"
msgstr ""
"Nameravate zapustiti program, ne da bi shranili svojo risbo.\n"
"Ali jo želite shraniti pred izhodom?\n"

#: contents/ui/dialogs/LinkDialog.qml:16
#: contents/ui/dialogs/LinkNoteDialog.qml:21
#, kde-format
msgctxt "@title:dialog"
msgid "Create your link"
msgstr "Ustvarite svojo povezavo"

#: contents/ui/dialogs/LinkDialog.qml:32 contents/ui/dialogs/URLDialog.qml:25
#, kde-format
msgctxt "@label:textbox, link URL, like the 'href' of an html <a> "
msgid "URL:"
msgstr "URL:"

#: contents/ui/dialogs/LinkDialog.qml:39
#: contents/ui/dialogs/LinkNoteDialog.qml:90
#, kde-format
msgctxt ""
"@label:textbox, the displayed text of a link, in html: <a>This text</a> "
msgid "Link text:"
msgstr "Besedilo povezave:"

#: contents/ui/dialogs/LinkNoteDialog.qml:37
#, kde-format
msgctxt "@label:combobox, 'a note' (the noun)"
msgid "Note:"
msgstr "Zabeležka:"

#: contents/ui/dialogs/LinkNoteDialog.qml:63
#, kde-format
msgctxt "@label:switch"
msgid "Search headers"
msgstr "Poišči glave"

#: contents/ui/dialogs/LinkNoteDialog.qml:75
#, kde-format
msgctxt "@label:textbox"
msgid "Header:"
msgstr "Glava:"

#: contents/ui/dialogs/NamingDialog.qml:23
#, kde-format
msgctxt "@title:dialog"
msgid "Choose a name"
msgstr "Izberite ime"

#: contents/ui/dialogs/NamingDialog.qml:47
#, kde-format
msgctxt "As in 'a name'"
msgid "Name:"
msgstr "Ime:"

#: contents/ui/dialogs/NamingErrorDialog.qml:20
#, kde-format
msgctxt ""
"@title:dialog, 'storage' as in 'the folder where all the notes will be "
"stored'"
msgid "KleverNotes Storage"
msgstr "Shramba KleverNotes"

#: contents/ui/dialogs/NamingErrorDialog.qml:33
#, kde-format
msgctxt ""
"@subtitle:dialog, %1 can be 'category' (a note category), 'group' (a note "
"group) or 'note' (a note)"
msgid "Your %1 name starts with a dot, "
msgstr "Vaše ime %1 se začne s piko, "

#: contents/ui/dialogs/NamingErrorDialog.qml:38
#, kde-format
msgctxt ""
"@subtitle:dialog, %1 can be 'category' (a note category), 'group' (a note "
"group) or 'note' (a note)"
msgid ""
"This %1 already exist.\n"
"Please choose another name for it.\n"
msgstr ""
"Ta %1 že obstaja.\n"
"Izberite drugo ime zanj_o.\n"

#: contents/ui/dialogs/StorageDialog.qml:16
#, kde-format
msgctxt ""
"Storage as in 'the folder where all the notes will be stored'; this text "
"will be followed by the path to this folder"
msgid "Storage created at "
msgstr "Shramba, ustvarjena ob "

#: contents/ui/dialogs/StorageDialog.qml:17
#, kde-format
msgctxt ""
"Storage as in 'the folder where all the notes will be stored'; this text "
"will be followed by the path to this folder"
msgid "Existing storage chosen at "
msgstr "Obstoječa shramba, izbrana v "

#: contents/ui/dialogs/StorageDialog.qml:22
#, kde-format
msgctxt ""
"@subtitle:dialog, Storage as in 'the folder where all the notes will be "
"stored'"
msgid ""
"It looks like this is your first time using this app!\n"
"\n"
"Please choose a location for your future KleverNotes storage or select an "
"existing one."
msgstr ""
"Kot kaže, je to prvič, da uporabljate ta program.\n"
"\n"
"Izberite mesto vaše bodoče shrambe KleverNotes ali izberite obstoječo."

#: contents/ui/dialogs/StorageDialog.qml:45
#, kde-format
msgctxt ""
"@label:button, Storage as in 'the folder where all the notes will be stored'"
msgid "Existing storage"
msgstr "Obstoječa shramba"

#: contents/ui/dialogs/StorageDialog.qml:47
#, kde-format
msgctxt ""
"@label:button, Storage as in 'the folder where all the notes will be stored'"
msgid "Change the storage path"
msgstr "Spremeni pot shrambe"

#: contents/ui/dialogs/StorageDialog.qml:61
#, kde-format
msgctxt ""
"@label:button, Storage as in 'the folder where all the notes will be stored'"
msgid "Create storage"
msgstr "Ustvari shrambo"

#: contents/ui/dialogs/StorageDialog.qml:63
#, kde-format
msgctxt "Storage as in 'the folder where all the notes will be stored'"
msgid "Create a new storage"
msgstr "Ustvarite novo shrambo"

#: contents/ui/dialogs/StorageDialog.qml:95
#, kde-format
msgctxt ""
"@subtitle:dialog, Storage as in 'the folder where all the notes will be "
"stored'"
msgid ""
"It looks like the selected folder is not a KleverNotes storage.\n"
"\n"
"Please choose a location for your future KleverNotes storage or select an "
"existing one."
msgstr ""
"Izbrana mapa ni videti shramba KleverNotes.\n"
"\n"
"Izberite mesto za vašo bodočo hrambo KleverNotes ali izberite obstoječo."

#: contents/ui/dialogs/tableMakerDialog/TableMakerDialog.qml:22
#, kde-format
msgctxt "@title:dialog, table => an html table"
msgid "Table creation"
msgstr "Ustvarjanje tabele"

#: contents/ui/dialogs/tableMakerDialog/TableMakerDialog.qml:43
#, kde-format
msgctxt ""
"@label, display the final size of the table %1 is the number of row and %2 "
"the number of column"
msgid "Size: %1 x %2"
msgstr "Velikost: %1 x %2"

#: contents/ui/dialogs/tableMakerDialog/TableMakerDialog.qml:155
#, kde-format
msgctxt "@label:button"
msgid "More options"
msgstr "Več možnosti"

#: contents/ui/dialogs/tableMakerDialog/TableMakerDialog.qml:209
#, kde-format
msgctxt "@label:spinbox, name, 'a row'"
msgid "Row"
msgstr "Vrstica"

#: contents/ui/dialogs/tableMakerDialog/TableMakerDialog.qml:221
#, kde-format
msgctxt "@label:spinbox, name, 'a column'"
msgid "Column"
msgstr "Stolpec"

#: contents/ui/dialogs/tableMakerDialog/TableMakerDialog.qml:233
#, kde-format
msgctxt "@label:combobox"
msgid "Alignment :"
msgstr "Poravnava:"

#: contents/ui/dialogs/tableMakerDialog/TableMakerDialog.qml:238
#, kde-format
msgctxt "@entry:combobox, alignment name"
msgid "Left"
msgstr "Levo"

#: contents/ui/dialogs/tableMakerDialog/TableMakerDialog.qml:239
#, kde-format
msgctxt "@entry:combobox, alignment name"
msgid "Center"
msgstr "Center"

#: contents/ui/dialogs/tableMakerDialog/TableMakerDialog.qml:240
#, kde-format
msgctxt "@entry:combobox, alignment name"
msgid "Right"
msgstr "Desno"

#: contents/ui/dialogs/todoDialog/TextAreaDelegate.qml:166
#, kde-format
msgctxt "@label %1 is current text length, %2 is maximum length of text field"
msgid "%1/%2"
msgstr "%1/%2"

#: contents/ui/dialogs/todoDialog/ToDoDialog.qml:17
#, kde-format
msgctxt "@title:dialog"
msgid "Add Todo"
msgstr "Dodaj opravek"

#: contents/ui/dialogs/todoDialog/ToDoDialog.qml:25
#, kde-format
msgctxt "@label:textbox"
msgid "Title:"
msgstr "Naslov:"

#: contents/ui/dialogs/todoDialog/ToDoDialog.qml:27
#, kde-format
msgctxt "@placeholderText:textbox"
msgid "Todo title (required)"
msgstr "Naslov opravka (zahtevano)"

#: contents/ui/dialogs/todoDialog/ToDoDialog.qml:39
#, kde-format
msgctxt "@label:textbox"
msgid "Description:"
msgstr "Opis:"

#: contents/ui/dialogs/todoDialog/ToDoDialog.qml:42
#, kde-format
msgctxt "@placeholderText:textbox"
msgid "Optional"
msgstr "Neobvezno"

#: contents/ui/dialogs/URLDialog.qml:14
#, kde-format
msgctxt "@title:dialog"
msgid "Choose an URL"
msgstr "Izberite URL"

#: contents/ui/dialogs/WarningDialog.qml:16
#, kde-format
msgctxt "@title:dialog"
msgid "Warning"
msgstr "Opozorilo"

#: contents/ui/dialogs/WarningDialog.qml:21
#, kde-format
msgctxt "@label:checkbox"
msgid "Do not show again"
msgstr "Ne prikazuj več"

#: contents/ui/dialogs/WarningDialog.qml:29
#, kde-format
msgid "This could cause visual artifact near the end of the pdf."
msgstr "To utegne povzročiti vizualni artefakt na koncu dokumenta pdf."

#: contents/ui/main.qml:18
#, kde-format
msgctxt "@title:ApplicationWindow"
msgid "KleverNotes"
msgstr "KleverNotes"

#: contents/ui/pages/MainPage.qml:24
#, kde-format
msgctxt "@title:page"
msgid "Welcome"
msgstr "Dobrodošli"

#: contents/ui/pages/MainPage.qml:74
#, kde-format
msgid "Welcome to KleverNotes!"
msgstr "Dobrodošli v KleverNotes!"

#: contents/ui/pages/MainPage.qml:84
#, kde-format
msgid "Create or select a note to start working !"
msgstr "Za pričetek z delom ustvarite ali izberite zabeležko!"

#: contents/ui/pages/PaintingPage.qml:29
#, kde-format
msgctxt "@title:page"
msgid "Paint!"
msgstr "Slikaj!"

#: contents/ui/pages/PaintingPage.qml:33 contents/ui/pages/PrintingPage.qml:71
#, kde-format
msgctxt "@label:button"
msgid "Save"
msgstr "Shrani"

#: contents/ui/pages/PaintingPage.qml:41
#, kde-format
msgctxt "@label:button, as in 'erase everything'"
msgid "Clear"
msgstr "Počisti"

#: contents/ui/pages/PaintingPage.qml:51
#, kde-format
msgctxt "@label:action"
msgid "Auto crop"
msgstr "Samodejno obreži"

#: contents/ui/pages/PrintingPage.qml:29
#, kde-format
msgctxt "@title:page"
msgid "Print"
msgstr "Natisni"

#: contents/ui/pages/PrintingPage.qml:39
#, kde-format
msgctxt "@label:ComboBox"
msgid "Color theme"
msgstr "Barvna tema"

#: contents/ui/pages/PrintingPage.qml:53
#, kde-format
msgctxt "@label:button, as in 'the background of an image'"
msgid "Background"
msgstr "Ozadje"

#: contents/ui/pages/SettingsPage.qml:19
#, kde-format
msgctxt "@title:window"
msgid "Settings"
msgstr "Nastavitve"

#: contents/ui/pages/SettingsPage.qml:25
#, kde-format
msgid ""
"Please choose a location for your future KleverNotes storage or select an "
"existing one.\n"
msgstr ""
"Izberite mesto za vašo bodočo hrambo KleverNotes ali izberite obstoječo.\n"

#: contents/ui/settings/AppearanceTab.qml:18
#, kde-format
msgctxt "@title, as in text editor"
msgid "Editor"
msgstr "Urejevalnik"

#: contents/ui/settings/AppearanceTab.qml:29
#, kde-format
msgctxt "@label:textbox, the font used in the text editor"
msgid "Editor font:"
msgstr "Pisava urejevalnika:"

#: contents/ui/settings/AppearanceTab.qml:38
#, kde-format
msgctxt "@title, where you can view the renderer note"
msgid "Note preview"
msgstr "Predogled opombe"

#: contents/ui/settings/DisplayPreview.qml:20
#, kde-format
msgctxt "@label:combobox"
msgid "Style:"
msgstr "Slog:"

#: contents/ui/settings/DisplayPreview.qml:47
#, kde-format
msgctxt "@label:button"
msgid "Text color:"
msgstr "Barva besedila:"

#: contents/ui/settings/DisplayPreview.qml:56
#, kde-format
msgctxt "@label:button"
msgid "Title color:"
msgstr "Barva naslova:"

#: contents/ui/settings/DisplayPreview.qml:66
#, kde-format
msgctxt "@label:button"
msgid "Link color:"
msgstr "Barva povezave:"

#: contents/ui/settings/DisplayPreview.qml:75
#, kde-format
msgctxt "@label:button"
msgid "Visited Link color:"
msgstr "Barva obiskane povezave:"

#: contents/ui/settings/DisplayPreview.qml:86
#, kde-format
msgctxt "@label:button"
msgid "Code color:"
msgstr "Barva kode:"

#: contents/ui/settings/DisplayPreview.qml:96
#, kde-format
msgctxt "@label:button"
msgid "Highlight color:"
msgstr "Barva označevanja:"

#: contents/ui/settings/DisplayPreview.qml:106
#, kde-format
msgctxt "@label:textbox"
msgid "General font:"
msgstr "Splošna pisava:"

#: contents/ui/settings/DisplayPreview.qml:117
#, kde-format
msgctxt "@label:textbox"
msgid "Code block font:"
msgstr "Pisava blokov kode:"

#: contents/ui/settings/GeneralTab.qml:21
#, kde-format
msgctxt ""
"@title, general storage settings, Storage as in 'the folder where all the "
"notes will be stored'"
msgid "Storage"
msgstr "Shramba"

#: contents/ui/settings/GeneralTab.qml:32
#, kde-format
msgctxt ""
"@label:textbox, Storage as in 'the folder where all the notes will be stored'"
msgid "Storage path:"
msgstr "Pot shrambe:"

#: contents/ui/settings/GeneralTab.qml:50
#, kde-format
msgctxt "@title, general sidebar settings"
msgid "Sidebar"
msgstr "Stranska letvica"

#: contents/ui/settings/GeneralTab.qml:64
#, kde-format
msgctxt "@label:textbox, the default note category name"
msgid "New Category name:"
msgstr "Novo ime kategorije:"

#: contents/ui/settings/GeneralTab.qml:94
#, kde-format
msgctxt "@label:textbox, the default note group name"
msgid "New Group name:"
msgstr "Novo ime skupine:"

#: contents/ui/settings/GeneralTab.qml:124
#, kde-format
msgctxt "@label:textbox, the default note name"
msgid "New Note name:"
msgstr "Novo ime zabeležke:"

#: contents/ui/settings/GeneralTab.qml:147
#, kde-format
msgctxt "@title, general text editor settings"
msgid "Editor"
msgstr "Urejevalnik"

#: contents/ui/settings/GeneralTab.qml:157
#, kde-format
msgctxt "@label:checkbox"
msgid "Use spaces for tab"
msgstr "Uporabi presledke za tabulator"

#: contents/ui/settings/GeneralTab.qml:169
#, kde-format
msgctxt "@label:combobox"
msgid "Number of spaces"
msgstr "Število presledkov"

#: contents/ui/settings/PluginsTab.qml:21
#, kde-format
msgctxt "@title"
msgid "Utility plugins"
msgstr "Uporabni vtičniki"

#: contents/ui/settings/PluginsTab.qml:33
#, kde-format
msgctxt "@label:checkbox"
msgid "Enable note linking"
msgstr "Omogoči povezovanje zapiskov"

#: contents/ui/settings/PluginsTab.qml:34
#, kde-format
msgctxt "@description:checkbox"
msgid "Note linking allows you to create a link from one note to another."
msgstr ""
"Povezovanje zapiskov vam omogoča, da ustvarite povezavo iz enega zapiska na "
"drugega."

#: contents/ui/settings/PluginsTab.qml:35
#, kde-format
msgctxt "@description:checkbox"
msgid "Advice: restart the app once activated."
msgstr "Nasvet: znova zaženite aplikacijo, ko je aktivirana."

#: contents/ui/settings/PluginsTab.qml:46
#, kde-format
msgctxt "@label:checkbox"
msgid "Enable PlantUML"
msgstr "Omogoči PlantUML"

#: contents/ui/settings/PluginsTab.qml:47
#, kde-format
msgctxt "@description:checkbox"
msgid "PlantUML let's you create diagram."
msgstr "PlantUML vam omogoča ustvarjanje diagrama."

#: contents/ui/settings/PluginsTab.qml:48
#, kde-format
msgctxt "@description:checkbox"
msgid ""
"Note: creating diagrams can be slow. If you have a large number of diagrams, "
"displaying them as images would be easier."
msgstr ""
"Opomba: ustvarjanje diagramov je lahko počasno. Če imate veliko število "
"diagramov, jih je lažje prikazali kot slike."

#: contents/ui/settings/PluginsTab.qml:58
#, kde-format
msgctxt "@lable:switch"
msgid "Enable dark background"
msgstr "Omogoči temno ozadje"

#: contents/ui/settings/PluginsTab.qml:59
#, kde-format
msgctxt "@description:switch"
msgid "PlantUML diagram will have a dark background."
msgstr "Diagram PlantUML bo imel temno ozadje."

#: contents/ui/settings/PluginsTab.qml:70
#, kde-format
msgctxt "@title"
msgid "Cosmetic plugins"
msgstr "Kozmetični vtičniki"

#: contents/ui/settings/PluginsTab.qml:82
#, kde-format
msgctxt "@label:checkbox"
msgid "Enable code syntax highlighting"
msgstr "Omogoči označevanje sintakse kode"

#: contents/ui/settings/PluginsTab.qml:83
#, kde-format
msgctxt "@description:checkbox"
msgid "List of supported highlighters"
msgstr "Seznam podprtih označevalnikov"

#: contents/ui/settings/PluginsTab.qml:93
#, kde-format
msgctxt "@label:combobox"
msgid "Highlighter"
msgstr "Označevalnik"

#: contents/ui/settings/PluginsTab.qml:121
#, kde-format
msgctxt "@label:combobox"
msgid "Highlighter style"
msgstr "Slog označevalnika"

#: contents/ui/settings/PluginsTab.qml:151
#, kde-format
msgctxt "@label:checkbox"
msgid "Enable quick emoji"
msgstr "Omogoči hitre čustvenčke"

#: contents/ui/settings/PluginsTab.qml:153
#, kde-format
msgctxt ""
"@description:checkbox, will be followed by the corresponding syntax, spacing "
"between the end of the sentence and the syntax is already there"
msgid "Quickly write emoji using the following syntax"
msgstr "Hitro napišite čustvenčka z naslednjo sintakso"

#: contents/ui/settings/PluginsTab.qml:153
#, kde-format
msgctxt "@exemple, something representing a possible emoji short name"
msgid "emoji_name"
msgstr "emoji_name"

#: contents/ui/settings/PluginsTab.qml:164
#, kde-format
msgctxt "@label:combobox"
msgid "Default emoji tone"
msgstr "Privzeti ton čustvenčkov"

#: contents/ui/settings/PluginsTab.qml:168
#, kde-format
msgctxt "@combobox:entry, As in 'default skin tone' (for an emoji), see '🫶'"
msgid "Default"
msgstr "Privzeto"

#: contents/ui/settings/PluginsTab.qml:169
#, kde-format
msgctxt "@combobox:entry, As in 'light skin tone' (for an emoji), see '🫶🏻'"
msgid "Light"
msgstr "Svetlo"

#: contents/ui/settings/PluginsTab.qml:170
#, kde-format
msgctxt ""
"@combobox:entry, As in 'medium light skin tone' (for an emoji), see '🫶🏼'"
msgid "Medium light"
msgstr "Srednje svetlo"

#: contents/ui/settings/PluginsTab.qml:171
#, kde-format
msgctxt "@combobox:entry, As in 'Medium skin tone' (for an emoji), see '🫶🏽'"
msgid "Medium"
msgstr "Srednje"

#: contents/ui/settings/PluginsTab.qml:172
#, kde-format
msgctxt ""
"@combobox:entry, As in 'Medium-dark skin tone' (for an emoji), see '🫶🏾'"
msgid "Medium dark"
msgstr "Srednje temno"

#: contents/ui/settings/PluginsTab.qml:173
#, kde-format
msgctxt "@combobox:entry, As in 'dark skin tone' (for an emoji), see '🫶🏿'"
msgid "Dark"
msgstr "Temno"

#: contents/ui/settings/PluginsTab.qml:187
#, kde-format
msgctxt "@lable:switch"
msgid "Enable quick emoji dialog"
msgstr "Omogoči hitro pogovorno okno emoji"

#: contents/ui/settings/PluginsTab.qml:188
#, kde-format
msgctxt "@description:switch"
msgid ""
"The emoji dialog insert the 'quick emoji' syntax instead of the actual emoji "
"inside the text."
msgstr ""
"Pogovorno okno emoji v besedilo vstavi sintakso 'hitri emoji' namesto "
"dejanskega emojija."

#: contents/ui/settings/TabBar.qml:13
#, kde-format
msgctxt "@label, tab (inside tab bar), general settings of the app"
msgid "General"
msgstr "Splošno"

#: contents/ui/settings/TabBar.qml:21
#, kde-format
msgctxt ""
"@label, tab (inside tab bar), all settings related to the general appearance "
"of the app"
msgid "Appearance"
msgstr "Videz"

#: contents/ui/settings/TabBar.qml:29
#, kde-format
msgctxt "@label, tab (inside tab bar), all things related to plugins"
msgid "Plugins"
msgstr "Vtičniki"

#: contents/ui/sideBar/ActionBar.qml:50
#, kde-format
msgctxt "as in 'A note category'"
msgid "Create a new category"
msgstr "Ustvarite novo kategorijo"

#: contents/ui/sideBar/ActionBar.qml:58
#, kde-format
msgctxt "as in 'A note group'"
msgid "Create a new group"
msgstr "Ustvarite novo skupino"

#: contents/ui/sideBar/ActionBar.qml:66
#, kde-format
msgctxt "as in 'A note'"
msgid "Create a new note"
msgstr "Ustvarite novo zabeležko"

#: contents/ui/sideBar/ActionBar.qml:74 contents/ui/sideBar/ContextMenu.qml:46
#, kde-format
msgid "Rename"
msgstr "Preimenuj"

#: contents/ui/sideBar/ActionBar.qml:87
#, kde-format
msgid "Search"
msgstr "Išči"

#: contents/ui/sideBar/ActionBar.qml:104
#, kde-format
msgid "Collapse sidebar"
msgstr "Strni stransko letvico"

#: contents/ui/sideBar/ActionBar.qml:105
#, kde-format
msgid "Expend sidebar"
msgstr "Razširi stransko letvico"

#: contents/ui/sideBar/ContextMenu.qml:18
#, kde-format
msgctxt "as in 'A note category'"
msgid "New category"
msgstr "Nova kategorija"

#: contents/ui/sideBar/ContextMenu.qml:28
#, kde-format
msgctxt "as in 'A note group'"
msgid "New group"
msgstr "Nova skupina"

#: contents/ui/sideBar/ContextMenu.qml:37
#, kde-format
msgctxt "as in 'A note'"
msgid "New note"
msgstr "Nova zabeležka"

#: contents/ui/sideBar/ContextMenu.qml:55
#, kde-format
msgid "Delete"
msgstr "Izbriši"

#: contents/ui/sideBar/SearchBar.qml:50
#, kde-format
msgid "From : "
msgstr "Od: "

#: contents/ui/sideBar/SearchBar.qml:82
#, kde-format
msgid "No search results"
msgstr "Ni zadetkov iskanja"

#: contents/ui/sideBar/Sidebar.qml:84
#, kde-format
msgid "Show notes"
msgstr "Prikaži zabeležke"

#: contents/ui/sideBar/Sidebar.qml:105 contents/ui/sideBar/Sidebar.qml:200
#, kde-format
msgid "Cheat sheet"
msgstr "Plonk listek"

#: contents/ui/sideBar/Sidebar.qml:121 contents/ui/sideBar/Sidebar.qml:212
#, kde-format
msgid "Settings"
msgstr "Nastavitve"

#: contents/ui/sideBar/Sidebar.qml:136 contents/ui/sideBar/Sidebar.qml:223
#, kde-format
msgid "About KleverNotes"
msgstr "O programu KleverNotes"

#: contents/ui/textEditor/BottomToolBar.qml:18
#, kde-format
msgctxt "@label:button, as in 'A note'"
msgid "Note"
msgstr "Zabeležka"

#: contents/ui/textEditor/BottomToolBar.qml:26
#, kde-format
msgctxt "@label:button"
msgid "TODO"
msgstr "OPRAVKI"

#: contents/ui/textEditor/CheatSheet.qml:18
#, kde-format
msgctxt "@window:title"
msgid "Markdown Cheat Sheet"
msgstr "Plonk listek za markdown"

#: contents/ui/textEditor/CheatSheet.qml:40
#, kde-format
msgid "Copied !"
msgstr "Kopirano!"

#: contents/ui/textEditor/CheatSheet.qml:47
#, kde-format
msgctxt "@title, cheat sheet section"
msgid "Basic syntax"
msgstr "Osnovna sintaksa"

#: contents/ui/textEditor/CheatSheet.qml:54
#, kde-format
msgid "Heading"
msgstr "Glava"

#: contents/ui/textEditor/CheatSheet.qml:55
#, kde-format
msgctxt "exemple"
msgid "Heading1"
msgstr "Glava 1"

#: contents/ui/textEditor/CheatSheet.qml:60
#, kde-format
msgid "Bold"
msgstr "Krepko"

#: contents/ui/textEditor/CheatSheet.qml:61
#, kde-format
msgctxt "exemple"
msgid "Bold text"
msgstr "Krepko besedilo"

#: contents/ui/textEditor/CheatSheet.qml:66
#, kde-format
msgid "Italic"
msgstr "Poševno"

#: contents/ui/textEditor/CheatSheet.qml:67
#, kde-format
msgctxt "exemple"
msgid "Italic text"
msgstr "Poševno besedilo"

#: contents/ui/textEditor/CheatSheet.qml:72
#, kde-format
msgid "Blockquote"
msgstr "Bločni sklic"

#: contents/ui/textEditor/CheatSheet.qml:73
#, kde-format
msgctxt "exemple"
msgid "Quote"
msgstr "Sklic"

#: contents/ui/textEditor/CheatSheet.qml:78
#, kde-format
msgid "Ordered list"
msgstr "Urejeni seznam"

#: contents/ui/textEditor/CheatSheet.qml:79
#: contents/ui/textEditor/CheatSheet.qml:86
#, kde-format
msgctxt "exemple"
msgid "First item"
msgstr "Prva postavka"

#: contents/ui/textEditor/CheatSheet.qml:80
#: contents/ui/textEditor/CheatSheet.qml:87
#, kde-format
msgctxt "exemple"
msgid "Second item"
msgstr "Druga postavka"

#: contents/ui/textEditor/CheatSheet.qml:85
#, kde-format
msgid "Unordered list"
msgstr "Neurejeni seznam"

#: contents/ui/textEditor/CheatSheet.qml:92
#, kde-format
msgid "Code"
msgstr "Koda"

#: contents/ui/textEditor/CheatSheet.qml:93
#, kde-format
msgctxt "exemple"
msgid "Inline code"
msgstr "Znotrajvrstčna koda"

#: contents/ui/textEditor/CheatSheet.qml:98
#, kde-format
msgctxt "@label, horizontal rule => <hr> html tag"
msgid "Rule"
msgstr "Pravilo"

#: contents/ui/textEditor/CheatSheet.qml:104
#, kde-format
msgid "Link"
msgstr "Povezava"

#: contents/ui/textEditor/CheatSheet.qml:105
#, kde-format
msgctxt "exemple"
msgid "title"
msgstr "naslov"

#: contents/ui/textEditor/CheatSheet.qml:105
#, kde-format
msgctxt "exemple"
msgid "https://www.example.com"
msgstr "https://www.example.com"

#: contents/ui/textEditor/CheatSheet.qml:110
#, kde-format
msgid "Image"
msgstr "Slika"

#: contents/ui/textEditor/CheatSheet.qml:111
#, kde-format
msgctxt "exemple"
msgid "alt text"
msgstr "alt besedilo"

#: contents/ui/textEditor/CheatSheet.qml:111
#, kde-format
msgctxt "exemple"
msgid "image"
msgstr "slika"

#: contents/ui/textEditor/CheatSheet.qml:118
#, kde-format
msgctxt "@title, cheat sheet section"
msgid "Extended syntax"
msgstr "Razširjena sintaksa"

#: contents/ui/textEditor/CheatSheet.qml:125
#, kde-format
msgid "Table"
msgstr "Tabela"

#: contents/ui/textEditor/CheatSheet.qml:126
#, kde-format
msgctxt "@label, noun, 'The left side of something'"
msgid "Left"
msgstr "Levo"

#: contents/ui/textEditor/CheatSheet.qml:126
#, kde-format
msgctxt "@label, verb, 'To center something'"
msgid "Center"
msgstr "Center"

#: contents/ui/textEditor/CheatSheet.qml:126
#, kde-format
msgctxt "@label, noun, 'The right side of something"
msgid "Right"
msgstr "Desno"

#: contents/ui/textEditor/CheatSheet.qml:128
#, kde-format
msgctxt "exemple"
msgid "Cell 1"
msgstr "Celica 1"

#: contents/ui/textEditor/CheatSheet.qml:128
#, kde-format
msgctxt "exemple"
msgid "Cell 2"
msgstr "Celica 2"

#: contents/ui/textEditor/CheatSheet.qml:128
#, kde-format
msgctxt "exemple"
msgid "Cell 3"
msgstr "Celica 3"

#: contents/ui/textEditor/CheatSheet.qml:133
#, kde-format
msgid "Fenced code block"
msgstr "Ograjen blok kode"

#: contents/ui/textEditor/CheatSheet.qml:135
#, kde-format
msgctxt "exemple"
msgid "Sample text here..."
msgstr "Vzorčno besedilo tukaj..."

#: contents/ui/textEditor/CheatSheet.qml:141
#, kde-format
msgid "Strikethrough"
msgstr "Prečrtano"

#: contents/ui/textEditor/CheatSheet.qml:142
#, kde-format
msgctxt "exemple, something wrong"
msgid "The world is flat."
msgstr "Svet je ploščat."

#: contents/ui/textEditor/CheatSheet.qml:147
#, kde-format
msgid "Task list"
msgstr "Seznam nalog"

#: contents/ui/textEditor/CheatSheet.qml:148
#, kde-format
msgctxt "exemple"
msgid "Make more foo"
msgstr "Naredi več foo"

#: contents/ui/textEditor/CheatSheet.qml:149
#, kde-format
msgctxt "exemple"
msgid "Make more bar"
msgstr "Naredi več bar"

#: contents/ui/textEditor/CheatSheet.qml:156
#, kde-format
msgctxt "@title, cheat sheet section"
msgid "KleverNotes plugins"
msgstr "Vtičniki KleverNotes"

#: contents/ui/textEditor/CheatSheet.qml:165
#, kde-format
msgid "Note link"
msgstr "Povezava zabeležke"

#: contents/ui/textEditor/CheatSheet.qml:167
#, kde-format
msgctxt ""
"exemple, a note path ('/' are important), Category as in 'a category, Group "
"'a group', Note 'a note'"
msgid "Category/Group/Note"
msgstr "Kategorija/Grupa/Zabeležka"

#: contents/ui/textEditor/CheatSheet.qml:168
#, kde-format
msgctxt "exemple"
msgid "header"
msgstr "glava"

#: contents/ui/textEditor/CheatSheet.qml:168
#, kde-format
msgctxt "exemple"
msgid "displayed name"
msgstr "prikazano ime"

#: contents/ui/textEditor/EditorView.qml:29
#, kde-format
msgctxt "@tooltip, will be followed by the shortcut"
msgid "Linked notes"
msgstr "Povezane zabeležke"

#: contents/ui/textEditor/EditorView.qml:40
#, kde-format
msgctxt "@tooltip, Print action, will be followed by the shortcut"
msgid "Print"
msgstr "Natisni"

#: contents/ui/textEditor/EditorView.qml:51
#, kde-format
msgctxt "@tooltip, will be followed by the shortcut"
msgid "View/Hide editor"
msgstr "Ogled/skrij urejevalnik"

#: contents/ui/textEditor/EditorView.qml:67
#, kde-format
msgctxt ""
"@tooltip, display as in 'the note preview', will be followed by the shortcut"
msgid "View/Hide preview"
msgstr "Ogled/skrij predogled"

#: contents/ui/textEditor/NotesMap.qml:19
#, kde-format
msgctxt "@window:title"
msgid "Linked notes map"
msgstr "Zemljevid povezanih zabeležk"

#: contents/ui/textEditor/NotesMap.qml:39
#, kde-format
msgctxt "@title, notes map section"
msgid "Existing notes"
msgstr "Obstoječe zabeležke"

#: contents/ui/textEditor/NotesMap.qml:65
#, kde-format
msgctxt "@title, notes map section"
msgid "Missing notes"
msgstr "Manjkajoče zabeležke"

#: contents/ui/textEditor/NotesMap.qml:77
#: contents/ui/textEditor/TextDisplay.qml:176
#, kde-format
msgctxt "@notification, error message %1 is a path"
msgid "%1 doesn't exists"
msgstr "%1 ne obstaja"

#: contents/ui/textEditor/TextToolBar.qml:36
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Headers"
msgstr "Glave"

#: contents/ui/textEditor/TextToolBar.qml:36
#, kde-format
msgctxt "@tooltip, short form of 'number'"
msgid "num"
msgstr "število"

#: contents/ui/textEditor/TextToolBar.qml:42
#, kde-format
msgctxt "@tooltip, text format header level, will be followed by the shortcut"
msgid "Header 1"
msgstr "Glava 1"

#: contents/ui/textEditor/TextToolBar.qml:54
#, kde-format
msgctxt "@tooltip, text format header level, will be followed by the shortcut"
msgid "Header 2"
msgstr "Glava 2"

#: contents/ui/textEditor/TextToolBar.qml:66
#, kde-format
msgctxt "@tooltip, text format header level, will be followed by the shortcut"
msgid "Header 3"
msgstr "Glava 3"

#: contents/ui/textEditor/TextToolBar.qml:78
#, kde-format
msgctxt "@tooltip, text format header level, will be followed by the shortcut"
msgid "Header 4"
msgstr "Glava 4"

#: contents/ui/textEditor/TextToolBar.qml:90
#, kde-format
msgctxt "@tooltip, text format header level, will be followed by the shortcut"
msgid "Header 5"
msgstr "Glava 5"

#: contents/ui/textEditor/TextToolBar.qml:102
#, kde-format
msgctxt "@tooltip, text format header level, will be followed by the shortcut"
msgid "Header 6"
msgstr "Glava 6"

#: contents/ui/textEditor/TextToolBar.qml:116
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Bold"
msgstr "Krepko"

#: contents/ui/textEditor/TextToolBar.qml:127
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Italic"
msgstr "Poševno"

#: contents/ui/textEditor/TextToolBar.qml:138
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Strikethrough"
msgstr "Prečrtano"

#: contents/ui/textEditor/TextToolBar.qml:149
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Code"
msgstr "Koda"

#: contents/ui/textEditor/TextToolBar.qml:160
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Quote"
msgstr "Sklic"

#: contents/ui/textEditor/TextToolBar.qml:171
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Image"
msgstr "Slika"

#: contents/ui/textEditor/TextToolBar.qml:180
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Link"
msgstr "Povezava"

#: contents/ui/textEditor/TextToolBar.qml:189
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Table"
msgstr "Tabela"

#: contents/ui/textEditor/TextToolBar.qml:198
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Ordered list"
msgstr "Urejeni seznam"

#: contents/ui/textEditor/TextToolBar.qml:211
#: contents/ui/textEditor/TextToolBar.qml:235
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Unordered list"
msgstr "Neurejeni seznam"

#: contents/ui/textEditor/TextToolBar.qml:224
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Text highlight"
msgstr "Označeno besedilo"

#: contents/ui/textEditor/TextToolBar.qml:247
#, kde-format
msgctxt "@tooltip, text format, will be followed by the shortcut"
msgid "Link note"
msgstr "Poveži zabeležko"

#: contents/ui/textEditor/TextToolBar.qml:327
#: contents/ui/textEditor/TextToolBar.qml:328
#: contents/ui/textEditor/TextToolBar.qml:329
#: contents/ui/textEditor/TextToolBar.qml:331
#: contents/ui/textEditor/TextToolBar.qml:332
#, kde-format
msgid "Header"
msgstr "Glava"

#: contents/ui/todoEditor/ToDoView.qml:23
#, kde-format
msgctxt "@label:button, checked as in 'the todos that are checked'"
msgid "Clear checked"
msgstr "Počisti odkljukano"

#: logic/kleverUtility.cpp:99
#, kde-format
msgid ""
"/*            \n"
"This file is a copy of the default CSS style for the note "
"display.            \n"
"Feel free to edit it or make your own.            \n"
"Note: each style need to have a different name.            \n"
"*/\n"
msgstr ""
"/*            \n"
"Ta datoteka je kopija privzetega sloga CSS za prikaz zabeležke.            \n"
"Prosto jo urejajte ali izdelajte svojo lastno.            \n"
"Opomba: vsak slog mora imeti drugačno ime.            \n"
"*/\n"

#: logic/plugins/emoji/emojiModel.cpp:152
#: logic/plugins/emoji/emojiModel.cpp:210
#, kde-format
msgctxt "Previously used emojis"
msgid "History"
msgstr "Zgodovina"

#: logic/plugins/emoji/emojiModel.cpp:157
#, kde-format
msgctxt "'Smileys' is a category of emoji"
msgid "Smileys"
msgstr "Smeškoti"

#: logic/plugins/emoji/emojiModel.cpp:162
#, kde-format
msgctxt "'People' is a category of emoji"
msgid "People"
msgstr "Ljudje"

#: logic/plugins/emoji/emojiModel.cpp:167
#, kde-format
msgctxt "'Nature' is a category of emoji"
msgid "Nature"
msgstr "Narava"

#: logic/plugins/emoji/emojiModel.cpp:172
#, kde-format
msgctxt "'Food' is a category of emoji"
msgid "Food"
msgstr "Hrana"

#: logic/plugins/emoji/emojiModel.cpp:177
#, kde-format
msgctxt "'Activities' is a category of emoji"
msgid "Activities"
msgstr "Dejavnosti"

#: logic/plugins/emoji/emojiModel.cpp:182
#, kde-format
msgctxt "'Travel' is  a category of emoji"
msgid "Travel"
msgstr "Potovanja"

#: logic/plugins/emoji/emojiModel.cpp:187
#, kde-format
msgctxt "'Objects' is a category of emoji"
msgid "Objects"
msgstr "Predmeti"

#: logic/plugins/emoji/emojiModel.cpp:192
#, kde-format
msgctxt "'Symbols' is a category of emoji"
msgid "Symbols"
msgstr "Simboli"

#: logic/plugins/emoji/emojiModel.cpp:197
#, kde-format
msgctxt "'Flags' is a category of emoji"
msgid "Flags"
msgstr "Zastave"

#: logic/plugins/emoji/emojiModel.cpp:216
#, kde-format
msgctxt "'Custom' is a category of emoji"
msgid "Custom"
msgstr "Po meri"

#: logic/printing/printingHelper.cpp:34
#, kde-format
msgid "An error occurred while trying to copy this pdf."
msgstr "Prišlo je do napake pri poskusu kopiranja te datoteke pdf."

#: logic/treeview/noteTreeModel.cpp:282
#, kde-format
msgid "An error occurred while trying to create the demo note."
msgstr "Prišlo je do napake pri poskusu ustvarjanja vzorčne zabeležke."

#: logic/treeview/noteTreeModel.cpp:418
#, kde-format
msgid "An error occurred while trying to create this item."
msgstr "Prišlo je do napake pri poskusu ustvarjanja te postavke."

#: logic/treeview/noteTreeModel.cpp:456
#, kde-format
msgid "An error occurred while trying to remove this item."
msgstr "Prišlo je do napake pri poskusu odstranjevanja te postavke."

#: logic/treeview/noteTreeModel.cpp:478
#, kde-format
msgid "An error occurred while trying to rename this item."
msgstr "Prišlo je do napake pri poskusu preimenovanja te postavke."

#: logic/treeview/noteTreeModel.cpp:573
#, kde-format
msgid "An error occurred while trying to create the note."
msgstr "Prišlo je do napake pri poskusu ustvarjanja zabeležke."

#: logic/treeview/noteTreeModel.cpp:584
#, kde-format
msgid "An error occurred while trying to create the group."
msgstr "Prišlo je do napake pri poskusu ustvarjanja skupine."

#: logic/treeview/noteTreeModel.cpp:594
#, kde-format
msgid "An error occurred while trying to create the category."
msgstr "Prišlo je do napake pri poskusu ustvarjanja kategorije."

#: logic/treeview/noteTreeModel.cpp:602
#, kde-format
msgid "An error occurred while trying to create the storage."
msgstr "Prišlo je do napake pri poskusu ustvarjanja shrambe."

#: main.cpp:57
#, kde-format
msgctxt "@title"
msgid "KleverNotes"
msgstr "KleverNotes"

#: main.cpp:61
#, kde-format
msgid "Application Description"
msgstr "Opis programa"

#: main.cpp:65
#, kde-format
msgid "(c) 2022-2023"
msgstr "(c) 2022-2023"

#: main.cpp:66
#, kde-format
msgctxt "@info:credit"
msgid "Louis Schul"
msgstr "Louis Schul"

#: main.cpp:71
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Matjaž Jeran, Martin Srebotnjak"

#: main.cpp:71
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "matjaz.jeran@amis.net, miles@filmsi.net"

#~ msgid "Name, as in 'A note'"
#~ msgstr "Ime, kot v »Zabeležka«"

#~ msgctxt ""
#~ "@title, display as in 'the note display' where you can visualize the note"
#~ msgid "Display"
#~ msgstr "Prikaži"

#~ msgctxt "@title"
#~ msgid "KleverNotes plugins"
#~ msgstr "Vtičniki KleverNotes"

#~ msgctxt "@label:textbox"
#~ msgid "Font:"
#~ msgstr "Pisava:"
