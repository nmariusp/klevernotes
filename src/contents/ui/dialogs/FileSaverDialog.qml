// SPDX-License-Identifier: GPL-2.0-or-later
// SPDX-FileCopyrightText: 2023 Louis Schul <schul9louis@gmail.com>

import QtQuick 2.15
import Qt.labs.platform 1.1

import org.kde.kirigami 2.19 as Kirigami

FileDialog {
    id: fileDialog

    property QtObject caller
    property string noteName

    title: i18nc("@title:dialog, choose the location of where the file will be saved", "Save note")

    folder: StandardPaths.writableLocation(StandardPaths.DocumentsLocation)
    currentFile: folder+"/"+noteName+".pdf"
    fileMode: FileDialog.SaveFile

    onAccepted: {
        caller.path = fileDialog.currentFile
    }
}
