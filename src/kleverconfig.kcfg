<?xml version="1.0" encoding="UTF-8"?>
<!--
    SPDX-License-Identifier: GPL-2.0-or-later
    SPDX-FileCopyrightText: 2022 Louis Schul <schul9louis@gmail.com>
-->
<kcfg xmlns="http://www.kde.org/standards/kcfg/1.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.kde.org/standards/kcfg/1.0
    http://www.kde.org/standards/kcfg/1.0/kcfg.xsd" >
    <kcfgfile name="KleverNotesrc" />
    <group name="General">
        <entry name="storagePath" type="Path">
            <label>Path to the ./KleverNotes folder</label>
            <default>None</default>
        </entry>

        <entry name="categoryDisplayName" type="String">
            <label>The default name shown in place of "./BaseCategory"</label>
            <default>Notes</default>
        </entry>
        <entry name="defaultCategoryName" type="String">
            <label>The default name for a new Category</label>
            <default>New Category</default>
        </entry>
        <entry name="defaultGroupName" type="String">
            <label>The default name for a new Group</label>
            <default>New Group</default>
        </entry>
        <entry name="defaultNoteName" type="String">
            <label>The default name for a new Note</label>
            <default>New Note</default>
        </entry>

        <entry name="pdfWarningHidden" type="Bool">
            <label>Whether or not the warning message for pdf background should be hidden</label>
            <default>false</default>
        </entry>

        <entry name="editorVisible" type="Bool">
            <label>Whether or not the editor is visible</label>
            <default>true</default>
        </entry>
        <entry name="previewVisible" type="Bool">
            <label>Whether or not the preview is visible</label>
            <default>true</default>
        </entry>

        <entry name="useSpaceForTab" type="Bool">
            <label>Whether or not the editor replace tab with space</label>
            <default>true</default>
        </entry>
        <entry name="spacesForTab" type="Int">
            <label>The number of spaces with which the tab is replaced</label>
            <default>4</default>
        </entry>
    </group>

    <group name="Appearance">
        <entry name="stylePath" type="Path">
            <label>Path to the note display css style</label>
            <default>:/KleverStyle.css</default>
        </entry>
        <entry name="editorFont" type="Font">
            <label>The default text editor font (if not specified default to system theme)</label>
        </entry>
        <entry name="codeFont" type="Font">
            <label>The default code block font</label>
        </entry>
        <entry name="viewFont" type="Font">
            <label>The default text view font (if not specified default to system theme)</label>
        </entry>
        <entry name="viewBodyColor" type="String">
            <label>The default text view body color (if not specified default to system theme)</label>
            <default>None</default>
        </entry>
        <entry name="viewTextColor" type="String">
            <label>The default text view text color (if not specified default to system theme)</label>
            <default>None</default>
        </entry>
        <entry name="viewTitleColor" type="String">
            <label>The default text view title color (if not specified default to system theme)</label>
            <default>None</default>
        </entry>
        <entry name="viewLinkColor" type="String">
            <label>The default text view link color (if not specified default to system theme)</label>
            <default>None</default>
        </entry>
        <entry name="viewVisitedLinkColor" type="String">
            <label>The default text view visited link color (if not specified default to system theme)</label>
            <default>None</default>
        </entry>
        <entry name="viewCodeColor" type="String">
            <label>The default text view code color (if not specified default to system theme)</label>
            <default>None</default>
        </entry>
        <entry name="viewHighlightColor" type="String">
            <label>The default text view highlight color (if not specified default to system theme)</label>
            <default>None</default>
        </entry>
    </group>

    <group name="Plugins">
        <entry name="pumlEnabled" type="Bool">
            <label>Whether or not PlantUML is enable</label>
            <default>false</default>
        </entry>
        <entry name="pumlDark" type="Bool">
            <label>Whether or not PlantUML diagram use dark background</label>
            <default>false</default>
        </entry>

        <entry name="quickEmojiEnabled" type="Bool">
            <label>Whether or not quick emoji (':emoji_name:') is enable</label>
            <default>false</default>
        </entry>
        <entry name="emojiTone" type="String">
            <label>The tone the user wants to use by default when tone is available for an emoji</label>
            <default>None</default>
        </entry>
        <entry name="quickEmojiDialogEnabled" type="Bool">
            <label>Whether or not the emoji dialog insert quick emoji (':emoji_name:')</label>
            <default>false</default>
        </entry>

        <entry name="codeSynthaxHighlightEnabled" type="Bool">
            <label>Whether or not the code block syntax highlighting is enable</label>
            <default>false</default>
        </entry>
        <entry name="codeSynthaxHighlighter" type="String">
            <label>The highlighter choosen by the user</label>
            <default></default>
        </entry>
        <entry name="codeSynthaxHighlighterStyle" type="String">
            <label>The highlighter style choosen by the user</label>
            <default></default>
        </entry>

        <entry name="noteMapEnabled" type="Bool">
            <label>Whether or not the note map is enable</label>
            <default>false</default>
        </entry>
    </group>
</kcfg>

