# h1 Heading 😎
## h2 Heading
### h3 Heading
#### h4 Heading
##### h5 Heading
###### h6 Heading


## Horizontal Rules

___

---

***


## Emphasis

**This is bold text**

__This is bold text__

*This is italic text*

_This is italic text_

~~Strikethrough~~


## Blockquotes


> Blockquotes can also be nested...
>> ...by using additional greater-than signs right next to each other...
> > > ...or with spaces between arrows.


## Lists

#### Unordered

+ Create a list by starting a line with `+`, `-`, or `*`
+ Sub-lists are made by indenting 2 spaces:
  - Marker character change forces new list start:
    * Ac tristique libero volutpat at
    + Facilisis in pretium nisl aliquet
    - Nulla volutpat aliquam velit
+ Very easy!

#### Ordered

1. Lorem ipsum dolor sit amet
2. Consectetur adipiscing elit
3. Integer molestie lorem at massa


1. You can use sequential numbers...
1. ...or keep all the numbers as `1.`


57. ... or tart numbering 
1. with offset

#### Tasks 

- [ ] Make more foo
- [x] Make more bar


# Code

Inline `code`

Indented code

    // Some comments
    line 1 of code
    line 2 of code
    line 3 of code


Block code "fences"

```C++
#include <iostream>

int main() {
    std::cout << "Hello World!";
    return 0;
}
```

## Tables

#### Default

| Company | Contact | Country |
| ------ | ------ | ------ |
| Alfreds Futterkiste | Maria Anders | Germany |
| Centro comercial Moctezuma | Francisco Chang | Mexico |
| Ernst Handel | Roland Mendel | Austria |

#### Only middle pipes are necessary  !

Company | Contact | Country 
------ | ------ | ------
Alfreds Futterkiste | Maria Anders | Germany
Centro comercial Moctezuma | Francisco Chang | Mexico 
Ernst Handel | Roland Mendel | Austria

#### Choose columns alignment

| Left | Center | Right |
| :------ | :------: | ------:|
| Alfreds Futterkiste | Maria Anders | Germany |
| Centro comercial Moctezuma | Francisco Chang | Mexico |
| Ernst Handel | Roland Mendel | Austria |


## Links

[link text](https://invent.kde.org/office/klevernotes)

[link with title](https://invent.kde.org/office/klevernotes "title text!")


## Images

![KDE](https://upload.wikimedia.org/wikipedia/commons/8/8d/KDE_logo.svg)

#### Like links, Images also have a footnote style syntax

![Alt text][id]

With a reference later in the document defining the URL location checkout the editor 😉

[id]: https://upload.wikimedia.org/wikipedia/commons/8/8d/KDE_logo.svg  "The KDE logo"

#### Image can also be stored locally with your note

![KleverNotes](./Images/logo.png "KleverNotes logo !") 

Using notation like "/home/user/myfile.png" or "~/myfile.png" will also work if you want to access image stored on your system but not alongside your note 😁

