// SPDX-FileCopyrightText 2023 Louis Schul <schul9louis@gmail.com>
// SPDX-License-Identifier: GPL-2.0-or-later

#include "qmlLinker.h"

QmlLinker::QmlLinker(QObject *parent)
    : QObject(parent)
{
}
