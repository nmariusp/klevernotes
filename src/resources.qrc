<!--
    SPDX-License-Identifier: CC0-1.0
    SPDX-FileCopyrightText: 2022 Louis Schul <schul9louis@gmail.com>
-->
<RCC>
    <qresource prefix="/">
        <file>contents/ui/main.qml</file>

        <file>contents/ui/dialogs/DeleteConfirmationDialog.qml</file>
        <file>contents/ui/dialogs/FilePickerDialog.qml</file>
        <file>contents/ui/dialogs/FileSaverDialog.qml</file>
        <file>contents/ui/dialogs/FolderPickerDialog.qml</file>
        <file>contents/ui/dialogs/FontPickerDialog.qml</file>
        <file>contents/ui/dialogs/LeavePaintingDialog.qml</file>
        <file>contents/ui/dialogs/LinkDialog.qml</file>
        <file>contents/ui/dialogs/LinkNoteDialog.qml</file>
        <file>contents/ui/dialogs/NamingDialog.qml</file>
        <file>contents/ui/dialogs/NamingErrorDialog.qml</file>
        <file>contents/ui/dialogs/StorageDialog.qml</file>
        <file>contents/ui/dialogs/URLDialog.qml</file>
        <file>contents/ui/dialogs/WarningDialog.qml</file>

        <file>contents/ui/dialogs/colorDialog/ColorDialog.qml</file>
        <file>contents/ui/dialogs/colorDialog/ColorPicker.qml</file>
        <file>contents/ui/dialogs/colorDialog/HSPicker.qml</file>
        <file>contents/ui/dialogs/colorDialog/LightnessSlider.qml</file>

        <file>contents/ui/dialogs/emojiDialog/EmojiDelegate.qml</file>
        <file>contents/ui/dialogs/emojiDialog/EmojiDialog.qml</file>
        <file>contents/ui/dialogs/emojiDialog/EmojiGrid.qml</file>
        <file>contents/ui/dialogs/emojiDialog/EmojiPicker.qml</file>
        <file>contents/ui/dialogs/emojiDialog/EmojiTonesPicker.qml</file>

        <file>contents/ui/dialogs/imagePickerDialog/ButtonDelegate.qml</file>
        <file>contents/ui/dialogs/imagePickerDialog/ImagePickerDialog.qml</file>

        <file>contents/ui/dialogs/tableMakerDialog/TableMakerDialog.qml</file>
        <file>contents/ui/dialogs/tableMakerDialog/UpDownButtonDelegate.qml</file>

        <file>contents/ui/dialogs/todoDialog/TextAreaDelegate.qml</file>
        <file>contents/ui/dialogs/todoDialog/ToDoDialog.qml</file>

        <file>contents/ui/pages/AboutPage.qml</file>
        <file>contents/ui/pages/MainPage.qml</file>
        <file>contents/ui/pages/PaintingPage.qml</file>
        <file>contents/ui/pages/PrintingPage.qml</file>
        <file>contents/ui/pages/SettingsPage.qml</file>

        <file>contents/ui/painting/ColorBar.qml</file>
        <file>contents/ui/painting/ColorButton.qml</file>

        <file>contents/ui/settings/AppearanceTab.qml</file>
        <file>contents/ui/settings/DisplayPreview.qml</file>
        <file>contents/ui/settings/FontPicker.qml</file>
        <file>contents/ui/settings/GeneralTab.qml</file>
        <file>contents/ui/settings/PluginsTab.qml</file>
        <file>contents/ui/settings/SettingsColorButton.qml</file>
        <file>contents/ui/settings/TabBar.qml</file>

        <file>contents/ui/sharedComponents/ExpendingFormSwitch.qml</file>

        <file>contents/ui/sideBar/ActionBar.qml</file>
        <file>contents/ui/sideBar/ContextMenu.qml</file>
        <file>contents/ui/sideBar/SearchBar.qml</file>
        <file>contents/ui/sideBar/Sidebar.qml</file>
        <file>contents/ui/sideBar/TreeItem.qml</file>
        <file>contents/ui/sideBar/TreeView.qml</file>

        <file>contents/ui/textEditor/components/CheatSheetEntry.qml</file>
        <file>contents/ui/textEditor/components/NotesMapEntry.qml</file>
        <file>contents/ui/textEditor/BottomToolBar.qml</file>
        <file>contents/ui/textEditor/CheatSheet.qml</file>
        <file>contents/ui/textEditor/EditorView.qml</file>
        <file>contents/ui/textEditor/NotesMap.qml</file>
        <file>contents/ui/textEditor/TextDisplay.qml</file>
        <file>contents/ui/textEditor/TextEditor.qml</file>
        <file>contents/ui/textEditor/TextToolBar.qml</file>

        <file>contents/ui/todoEditor/TodoDelegate.qml</file>
        <file>contents/ui/todoEditor/ToDoView.qml</file>

        <file alias="index.html">contents/resources/index.html</file>
        <file alias="qwebchannel.js">contents/resources/qwebchannel.js</file>

        <file alias="Avenir.css">contents/resources/style/Avenir.css</file>
        <file alias="KleverStyle.css">contents/resources/style/KleverStyle.css</file>
        <file alias="Style7.css">contents/resources/style/Style7.css</file>
        <file alias="Style9.css">contents/resources/style/Style9.css</file>

        <file alias="demo_note.md">example/Demo/note.md</file>
        <file alias="Images/logo.png">example/Demo/Images/logo.png</file>
    </qresource>
</RCC>
